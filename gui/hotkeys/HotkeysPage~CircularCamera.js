init = new Proxy(init, {apply: function(target, thisArg, args) {
    const hotkeys = new CircularCameraHotkeys();
    hotkeys.createDefaultSettingsIfNotExist();

    target(...args);
}});
