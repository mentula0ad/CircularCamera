var g_CircularCamera;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    g_CircularCamera = new CircularCamera();
}});
