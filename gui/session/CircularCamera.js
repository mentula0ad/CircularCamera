class CircularCamera
{

    constructor()
    {
        this.settings;
        this.init();

        this.radius = +this.settings.radius;
    }

    init()
    {
        // Save default settings if they don't exist
        const settings = new CircularCameraSettings();
        settings.createDefaultSettingsIfNotExist();
        // Save default hotkeys if they don't exist
        const hotkeys = new CircularCameraHotkeys();
        hotkeys.createDefaultSettingsIfNotExist();
        // Store settings
        const savedSettings = settings.getSaved();
        this.settings = settings.trim(savedSettings);
    }

    getPosition(index)
    {
        // Skip if map size is not set
        if (!g_SimState.mapSize)
            return;

        // Find unitary positions
        const radius = this.radius;
        const sq = Math.sqrt(2)/2;
        let position = {x: 0, z: 0};
        if (index == 0)
            position = {x: 1-sq*radius, z: 1+sq*radius};
        else if (index == 1)
            position = {x: 1, z: 1+radius};
        else if (index == 2)
            position = {x: 1+sq*radius, z: 1+sq*radius};
        else if (index == 3)
            position = {x: 1-radius, z: 1};
        else if (index == 4)
            position = {x: 1, z: 1};
        else if (index == 5)
            position = {x: 1+radius, z: 1};
        else if (index == 6)
            position = {x: 1-sq*radius, z: 1-sq*radius};
        else if (index == 7)
            position = {x: 1, z: 1-radius};
        else if (index == 8)
            position = {x: 1+sq*radius, z: 1-sq*radius};

        // Rotate
        const angle = Engine.GetCameraRotation().y;
        const cos = Math.cos(angle);
        const sin = Math.sin(angle);
        const oldPos = Object.assign({}, position);
        position.x = cos*(oldPos.x - 1) + sin*(oldPos.z - 1) + 1;
        position.z = cos*(oldPos.z - 1) - sin*(oldPos.x - 1) + 1;

        // Scale
        const mapRadius = g_SimState.mapSize/2;
        position.x *= mapRadius;
        position.z *= mapRadius;

        return position;
    }

    jump(index)
    {
        const position = this.getPosition(index);
	Engine.CameraMoveTo(position.x, position.z);
    }

    increaseRadius()
    {
        const speed = +this.settings.speed;
        this.radius = Math.min(1, this.radius + speed);
    }

    decreaseRadius()
    {
        const speed = +this.settings.speed;
        this.radius = Math.max(0, this.radius - speed);
    }

    resetRadius()
    {
        this.radius = +this.settings.radius;
    }

}
