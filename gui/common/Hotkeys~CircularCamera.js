/**
 * This class is responsible for dealing with hotkeys.
 */
class CircularCameraHotkeys
{

    constructor()
    {
        this.defaultHotkeys = {
            "hotkey.circularcamera.jump.1": "CapsLock+Q",
            "hotkey.circularcamera.jump.2": "CapsLock+W",
            "hotkey.circularcamera.jump.3": "CapsLock+E",
            "hotkey.circularcamera.jump.4": "CapsLock+A",
            "hotkey.circularcamera.jump.5": "CapsLock+S",
            "hotkey.circularcamera.jump.6": "CapsLock+D",
            "hotkey.circularcamera.jump.7": "CapsLock+Z",
            "hotkey.circularcamera.jump.8": "CapsLock+X",
            "hotkey.circularcamera.jump.9": "CapsLock+C",
            "hotkey.circularcamera.radius.increase": "CapsLock+Plus",
            "hotkey.circularcamera.radius.decrease": "CapsLock+Minus",
            "hotkey.circularcamera.radius.reset": "CapsLock+R"
        };
    }

    createDefaultSettingsIfNotExist()
    {
        const settings = this.defaultHotkeys;
        Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
        Engine.ConfigDB_SaveChanges("user");
        Engine.ReloadHotkeys();
    }

}
