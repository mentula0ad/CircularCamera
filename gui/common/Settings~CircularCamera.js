/**
 * This class is responsible for dealing with settings.
 */
class CircularCameraSettings
{

    getSaved()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/options/options~CircularCamera.json");
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = Engine.ConfigDB_GetValue("user", option.config)));
        return settings;
    }

    getDefault()
    {
        const optionsJSON = Engine.ReadJSONFile("gui/options/options~CircularCamera.json");
        let settings = {};
        optionsJSON.forEach(category => category.options.forEach(option => settings[option.config] = option.val.toString()));
        return settings;
    }

    createDefaultSettingsIfNotExist()
    {
        const settings = this.getDefault();
        Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
        Engine.ConfigDB_SaveChanges("user");
    }

    trim(settings)
    {
        const trimSize = "circularcamera.".length;
        return Object.fromEntries(Object.entries(settings).map(x => [x[0].substring(trimSize), x[1]]));
    }

}
