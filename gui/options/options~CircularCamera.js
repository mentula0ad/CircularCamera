init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    const options = Engine.ReadJSONFile("gui/options/options~CircularCamera.json");
    g_Options = g_Options.concat(options);

    // TODO: translation. The expected command (below) raises an error
    // translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions
    );

    // Write default values if missing
    const settings = new CircularCameraSettings();
    settings.createDefaultSettingsIfNotExist();
}});
