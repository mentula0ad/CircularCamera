# About CircularCamera

*CircularCamera* is a [0 A.D.](https://play0ad.com) mod providing extra hotkeys for quickly navigating the map using additional cameras.

# Features

* Nine new pre-set cameras are available: one points at the center of the map and eight point at the eight corners of the map.
* Hotkeys are available for each camera position. Hotkeys can be edited from the `Hotkeys` menu.
* Built-in cameras will work as usual; the new cameras will not conflict.

# How to:

* **Change hotkeys:** open the `Hotkeys` menu and filter by the keyword `circularcamera`. All relevant hotkeys can be found there.
* **Increase/decrease the distance of cameras from the center of the map:** open the `Options > CircularCamera` menu. All relevant settings can be found there.

# Installation

[Click here](https://gitlab.com/mentula0ad/CircularCamera/-/releases/permalink/latest/downloads/circularcamera.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/CircularCamera/-/releases/permalink/latest/downloads/circularcamera.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/CircularCamera/-/releases/permalink/latest/downloads/circularcamera.zip) | [Older Releases](https://gitlab.com/mentula0ad/CircularCamera/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/80735-circularcamera-mod-quickly-navigate-map-using-extra-cameras/).
